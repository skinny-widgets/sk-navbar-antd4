
import { AntdSkNavbar }  from '../../sk-navbar-antd/src/antd-sk-navbar.js';

export class Antd4SkNavbar extends AntdSkNavbar {

    get prefix() {
        return 'antd4';
    }

}
