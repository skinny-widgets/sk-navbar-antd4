# Skinny Widgets Navbar for Antd4 Theme


navbar element

```
npm i sk-navbar sk-navbar-antd4 --save
```

then add the following to your html

```html
<sk-config
    theme="antd"
    base-path="/node_modules/sk-core/src"
    theme-path="/node_modules/sk-theme-antd4"
></sk-config>
<sk-navbar>
    Nabar Contents
</sk-navbar>
<script type="module">
    import { SkNavbar } from './node_modules/sk-navbar/index.js';

    customElements.define('sk-navbar', SkNavbar);

</script>
```

you can enable panel and specify template for contents as path or using caching mechanics

```html
<sk-navbar id="navbar1" align="top" panel panel-tpl-path="fakepath">
    Some top Contents
    Bar
    <template id="SkNavbarPanelTpl">
        <a href="/foo">foo</a>
        <a href="/bar">bar</a>
    </template>
</sk-navbar>
```

#### attributes

**open** - identifies and controls if navbar is open

**align** - can be "right", "left", "buttom" and "top" to define where navbar is nested (default: "left")

**auto-open** - enables auto open and close on mouse move close to navbar side

**panel** - show panel

**panel-tpl-path** - specify custom tpl path for panel (e.g. panel-tpl-path="/panel.tpl.html"), if not
specified nothing is rendered

**sticky** - by default panels are sticky, but you can disable this by setting it to "false"

**clength** - contents pane width or height

#### template

id: SkNavbarTpl